package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetWithTwoObstacles() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testLandingRoverEmpityCommandString() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="";
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(0,0,N)", rover.executeCommand(command));
	}
	
	@Test
	public void testRoverTurningRight() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="r";
		
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(0,0,E)", rover.executeCommand(command));
	}
	
	@Test
	public void testRoverTurningLeft() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="l";
		
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(0,0,W)", rover.executeCommand(command));
	}
	
	@Test
	public void testMovingForwardOfOneCell() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="f";
		rover.setStatusX(7);
		rover.setStatusY(6);
		rover.setDirection("N");
		
		assertEquals("(7,7,N)", rover.executeCommand(command));
	}
	
	@Test
	public void testMovingBackwardOfOneCell() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="b";
		rover.setStatusX(5);
		rover.setStatusY(8);
		rover.setDirection("E");
		
		assertEquals("(4,8,E)", rover.executeCommand(command));
	}
	
	@Test
	public void testMovingCombined() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="ffrff";
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(2,2,E)", rover.executeCommand(command));
	}
	
	@Test
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="b";
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(0,9,N)", rover.executeCommand(command));
	}
	
	@Test
	public void testMovingCombinedWithOneObstacle() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		planetObstacles.add("2,2");
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="ffrfff";
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand(command));
	}
	

	@Test
	public void testMovingCombinedWithTwoObstacles() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		planetObstacles.add("2,2");
		planetObstacles.add("2,1");
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="ffrfffrflf";
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand(command));
	}
	
	@Test
	public void testMovingCombinedWithOneObstacleEdge() throws MarsRoverException {
		List<String> planetObstacles=new ArrayList<>();
		planetObstacles.add("0,9");
		
		MarsRover rover=new MarsRover(10,10, planetObstacles);
		String command="b";
		rover.setStatusX(0);
		rover.setStatusY(0);
		rover.setDirection("N");
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand(command));
	}

}
